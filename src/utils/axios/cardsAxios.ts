import axios from "axios";

const axiosCreate = axios.create({
    timeout: 5000
});

export const cardsAxios = {
    get: async () => {
        try {
            const response = await axiosCreate.get("https://6075786f0baf7c0017fa64ce.mockapi.io/products");
            return response.data;
        } catch (error) {
            console.log(error);
            return error;
        }
    }
}