export interface CardsStateInterface {
    cards: any[];
}

export interface CardsActionInterface {
    type: string;
    payload?: any;
}

export interface CardProps {
    id: string;
    oldPrice: string;
    price: string;
    title: string;
    seen: boolean;
    locality: string;
    date: number;
    image: string;
}