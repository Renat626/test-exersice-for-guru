import React from "react";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { useActions } from "../../hooks/useActions";
import { useEffect } from "react";
import CardItem from "./CardItem";
import styledComponents from "styled-components";
import { useState } from "react";

const Container = styledComponents.section`
    margin: 0 auto;
    padding-bottom: 400px;
    width: 100%;
    max-width: 968px;
    @media screen and (max-width: 1000px) {
        max-width: 710px;
    };
    @media screen and (max-width: 750px) {
        max-width: 470px;
    };
    @media screen and (max-width: 500px) {
        max-width: 300px;
    };
`;

const Headline = styledComponents.h1`
    margin-left: 32px;
    font-family: 'Ubuntu';
    font-size: 22px;
    line-height: 25px;
    color: #2C2C2C;
    margin-bottom: 8px;
`;

const ContainerForCard = styledComponents.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-column-gap: 24px;
    grid-row-gap: 24px;
    margin-bottom: 16px;
    @media screen and (max-width: 1000px) {
        grid-template-columns: repeat(3, 1fr);
    };
    @media screen and (max-width: 750px) {
        grid-template-columns: repeat(2, 1fr);
    };
    @media screen and (max-width: 500px) {
        grid-template-columns: repeat(1, 1fr);
    };
`;

const ContainerForButton = styledComponents.div`
    display: flex;
    justify-content: flex-end;
`;

const ButtonShowAll = styledComponents.button`
    border: none;
    background: none;
    font-family: 'Ubuntu';
    font-weight: 500;
    font-size: 14px;
    line-height: 16px;
    color: #00A0AB;
    cursor: pointer;
    display: flex;
    align-items: center;
`; 

const Cards: React.FC = () => {
    const cards = useTypedSelector(state => state.cards);
    const {cardsActionCreatorsGet} = useActions();
    const [count, setCount] = useState<number>(20);

    useEffect(() => {
        cardsActionCreatorsGet();
    }, [])

    if (cards.length !== 0) {
        for (let i = 0; i < cards.length; i++) {
            if (i === 0) {
                cards[i].image = "image1";
            }

            if (i % 2 !== 0) {
                cards[i].image = "image2";
            }
            
            if (i % 2 === 0) {
                cards[i].image = "image1";
            }
        }
    }

    const checkPosition = () => {
        const height = document.body.offsetHeight;
        const screenHeight = window.innerHeight;
        const scrolled = window.scrollY;
        const threshold = height - screenHeight / 4;
        const position = scrolled + screenHeight;

        if (position >= threshold) {
            if (count < cards.length) {
                changeCount();
            }
        } else {
        }
    }

    window.addEventListener('scroll', checkPosition);
    window.addEventListener('resize', checkPosition);

    function changeCount(): void {
        setCount(count + 10);
    }

    function showAll(): void {
        setCount(cards.length); 
        window.removeEventListener('scroll', checkPosition);
        window.removeEventListener('resize', checkPosition);
    }

    return (
        <Container>
            <Headline>
                Похожие объявления
            </Headline>

            <ContainerForCard>
                {cards.length !== 0 && (
                    cards.map((card, index) => {
                        if (count >= index + 1) {
                            return <CardItem key={card.id} id={card.id} oldPrice={card.oldPrice} price={card.price} title={card.title} seen={card.seen} locality={card.locality} date={card.date} image={card.image} />
                        }
                    })   
                )}
            </ContainerForCard>

            <ContainerForButton>
                <ButtonShowAll onClick={showAll}>
                    Показать еще

                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.11707 9.954L12.0711 17.1421L19.0251 9.954" stroke="#00A0AB" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </ButtonShowAll>
            </ContainerForButton>
        </Container>
    )
}

export default Cards;