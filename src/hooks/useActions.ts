import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import * as cardsActionCreators from "../store/actionCreators/cardsActionCreators";

export const useActions = () => {
    const dispatch = useDispatch();
    return bindActionCreators(cardsActionCreators, dispatch);
}