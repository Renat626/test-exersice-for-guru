import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import { cardsReducer } from "./reducers/cardsReducer";

export const store = createStore(cardsReducer, applyMiddleware(thunk));

export type RootState = ReturnType<typeof cardsReducer>;