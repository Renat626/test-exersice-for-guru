import { cardsAxios } from "../../utils/axios/cardsAxios";
import { cardsAction } from "../actions/cardsAction";
import { Dispatch } from "react";
import { CardsActionInterface } from "../../types/cardsType";
// import 

export const cardsActionCreatorsGet = () => async (dispatch: Dispatch<CardsActionInterface>) => {
    try {
        const response = await cardsAxios.get();
        await dispatch({type: cardsAction.GET_ALL, payload: response});
    } catch (error) {
        console.log(error);
    }
}