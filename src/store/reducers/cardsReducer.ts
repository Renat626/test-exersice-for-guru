import { CardsActionInterface } from './../../types/cardsType';
import { cardsState } from "../states/cardsState";
import { CardsStateInterface } from "../../types/cardsType";
import { cardsAction } from "../actions/cardsAction";

export const cardsReducer = (state = cardsState, action: CardsActionInterface): CardsStateInterface => {
    switch (action.type) {
        case cardsAction.GET_ALL:
            return {cards: action.payload}
        default:
            return {cards: []}
    }
}